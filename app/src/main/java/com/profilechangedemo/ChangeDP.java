package com.profilechangedemo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;

public class ChangeDP extends AppCompatActivity {

    ImageView showImage;
    Button submit;

    MainActivity abc = new MainActivity();
    String PhotoID;
    public String url;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    private Context mContext;
    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_dp);

        showImage = (ImageView) findViewById(R.id.imageView);
        submit = (Button) findViewById(R.id.profilePic);

        showImage.setImageBitmap(abc.bm);
        showImage.setImageBitmap(abc.thumbnail);


        final AccessToken accessToken = AccessToken.getCurrentAccessToken();



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PhotoID = getIntent().getExtras().getString("PhotoID");
                url = "https://m.facebook.com/photo.php?fbid="+PhotoID+"&id="+accessToken.getUserId()+"&prof&ls=your_photo_permalink&ref_component=mbasic_photo_permalink&ref_page=%2Fwap%2Fphoto.php&ref=bookmarks";

                webview = (WebView)findViewById(R.id.webview);

                FacebookSdk.sdkInitialize(getApplicationContext());

                LoginManager.getInstance();

                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.setAcceptCookie(true);

                webview.setWebViewClient(new UriWebViewClient());
                webview.setWebChromeClient(new UriChromeClient());
                webview.loadUrl(url);

                mContext = getApplicationContext();

            }
        });
    }

    private class UriWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            //Log.d("shouldOverrideUrlLoading", url);
            if (host.equals(url))
            {
                // This is my web site, so do not override; let my WebView load
                // the page
                if(webview!=null)
                {
                    webview.setVisibility(View.GONE);
                    webview.removeView(webview);
                    webview=null;
                }
                return false;
            }

            if(host.equals("m.facebook.com"))
            {
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch
            // another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            Log.e("onReceivedSslError", "onReceivedSslError");
            //super.onReceivedSslError(view, handler, error);
        }
    }

    class UriChromeClient extends WebChromeClient {

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
            webview = new WebView(mContext);
            webview.setVerticalScrollBarEnabled(false);
            webview.setHorizontalScrollBarEnabled(false);
            webview.setWebViewClient(new UriWebViewClient());
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setSavePassword(false);
            webview.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            webview.addView(webview);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(webview);
            resultMsg.sendToTarget();

            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow", "called");
        }

    }
}




